#ifndef CRATEFIRMEAREUPDATEEU_H
#define CRATEFIRMEAREUPDATEEU_H

#include <QObject>
#include "ltr/include/ltr030api.h"

class CrateFirmeareUpdateEU : public QObject {
    Q_OBJECT
public:
    CrateFirmeareUpdateEU(TLTR030 crate);

public slots:
    void startDspUpdate(QString filename);

signals:
    void opStart(unsigned full_size);
    void opProgress(QString stage, unsigned done);
    void opFinish(int err);

private:
    void fwProgress(DWORD stage, DWORD done_size, DWORD full_size);
    static void fw_cb(DWORD stage, DWORD done_size, DWORD full_size, void* cb_data);

    TLTR030 m_crate;
};

#endif // CRATEFIRMEAREUPDATEEU_H
