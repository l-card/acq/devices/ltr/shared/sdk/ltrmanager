#ifndef LTRMANAGERENV_H
#define LTRMANAGERENV_H

#include <QObject>
#include <QList>
#include <QModelIndex>


class ActionPlugin;
class LtrCratesModel;
class QAction;
class LtrdConnection;
class LtrCrateInfo;
class QWidget;


class LtrManagerEnv : public QObject {
    Q_OBJECT
public:
    enum ActionGroup {
        ActionGroupCrateInstance
    };


    enum ActionType {
        ActionTypeCrateConfig,
        ActionTypeCrateReset,
        ActionTypeCrateSyncCtl,
        ActionTypeCrateBoot
    };

    ~LtrManagerEnv();

    void registerActionPlugin(ActionType type, ActionPlugin *plugin);

    QWidget *mainWidget() const {return m_win;}

    LtrdConnection *currentLtrdCon() const {return m_curCon;}
    LtrCrateInfo   *currentCrate() const {return m_curCrate;}



    LtrCratesModel* cratesModel() const {return m_cratesModel;}


    QAction *action(ActionType type) const;
    QList<QAction *> actions(ActionGroup group) const;

private slots:
    void crateSelectionChanged(QModelIndex current, QModelIndex previous);

    void retranslate();
    void onActionTriggered();
private:
    class ActionDescr;

    QAction *createAction(ActionType type, ActionGroup group);
    ActionDescr *actionDesc(ActionType type) const;

    LtrdConnection *m_curCon;
    LtrCrateInfo  *m_curCrate;
    int m_curSlot;


    LtrManagerEnv(QWidget *win);
    LtrCratesModel *m_cratesModel;


    QWidget *m_win;
    QList<ActionDescr *> m_actionDescrs;


    friend class MainWindow;
};

#endif // LTRMANAGERENV_H
