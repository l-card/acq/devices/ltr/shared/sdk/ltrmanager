<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>AddIpEntryDialog</name>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddIpEntryDialog.h" line="114"/>
        <source>IP address addition</source>
        <translation>Добавление IP-адреса</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddIpEntryDialog.h" line="115"/>
        <source>IP address</source>
        <translation>IP-адрес</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddIpEntryDialog.h" line="118"/>
        <source>Connect now</source>
        <translation>Подключить сейчас</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddIpEntryDialog.h" line="119"/>
        <source>Automatic connect at startup</source>
        <translation>Автоматически подключать при запуске</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddIpEntryDialog.h" line="120"/>
        <source>Reconnection on error</source>
        <translation>Повторное подключение при ошибке</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddIpEntryDialog.h" line="121"/>
        <source>Save in ltrd settings</source>
        <translation>Сохранить в настройках ltrd</translation>
    </message>
</context>
<context>
    <name>AddLtrdConnectionDialog</name>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddLtrdConnectionDialog.h" line="112"/>
        <source>Remote connection</source>
        <translation>Удаленное подключение</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddLtrdConnectionDialog.h" line="115"/>
        <source>Connect now</source>
        <translation>Подключить сейчас</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_AddLtrdConnectionDialog.h" line="116"/>
        <source>Connect at startup</source>
        <translation>Подключать при запуске</translation>
    </message>
    <message>
        <location filename="../src/AddLtrdConnectionDialog/AddLtrdConnectionDialog.cpp" line="11"/>
        <source>Remote %1 IP address</source>
        <translation>IP-адрес удаленного %1</translation>
    </message>
    <message>
        <location filename="../src/AddLtrdConnectionDialog/AddLtrdConnectionDialog.cpp" line="12"/>
        <source>New %1 connection addition</source>
        <translation>Добавление нового соединения с %1</translation>
    </message>
</context>
<context>
    <name>ConfigPasswordDialog</name>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_ConfigPasswordDialog.h" line="76"/>
        <source>Changes saving</source>
        <translation>Сохранение изменений</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_ConfigPasswordDialog.h" line="77"/>
        <source>Enter password to write new crate settings.</source>
        <translation>Введите пароль, чтобы записать новые настройки крейта.</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_ConfigPasswordDialog.h" line="78"/>
        <source>(leave empty string if password not set)</source>
        <translation>(оставьте пустую строку, если пароль не был установлен)</translation>
    </message>
</context>
<context>
    <name>CrateConfigDialog</name>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="52"/>
        <source>Get IP-paramtes automatically</source>
        <translation>Получать параметры IP автоматически</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="69"/>
        <source>Factory MAC-address</source>
        <translation>Заводской MAC-адрес</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="77"/>
        <source>Enable user MAC-address</source>
        <translation>Разрешить пользовательский MAC-адрес</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="88"/>
        <source>User MAC-address</source>
        <translation>Пользовательский MAC-адрес</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="96"/>
        <source>Set new password</source>
        <translation>Установить новый пароль</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="103"/>
        <source>New password</source>
        <translation>Новый пароль</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="110"/>
        <source>Repeat new password</source>
        <translation>Повтор нового пароля</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="187"/>
        <source>Changes saving</source>
        <translation>Сохранение изменений</translation>
    </message>
    <message>
        <location filename="../src/Crates/ConfigDialog/CrateConfigDialog.cpp" line="188"/>
        <source>Write new crate settings?</source>
        <translation>Записать новые настройки крейта?</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="163"/>
        <source>Crate Settings</source>
        <translation>Настройки крейта</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="167"/>
        <source>Unknown interface</source>
        <translation>Неизвествный интерфейс</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="168"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="169"/>
        <source>TCP/IP (Ethernet)</source>
        <translation>TCP/IP (Ethernet)</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="171"/>
        <source>Crate IP address</source>
        <translation>IP-адрес крейта</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="172"/>
        <source>IP subnet mask</source>
        <translation>Маска подсети</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="173"/>
        <source>IP Gateway</source>
        <translation>Шлюз</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="174"/>
        <source>Additional settings</source>
        <translation>Дополнительные настройки</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateConfigDialog.h" line="164"/>
        <source>Crate interface</source>
        <translation>Интерфейс крейта</translation>
    </message>
</context>
<context>
    <name>CrateFirmeareUpdateEU</name>
    <message>
        <location filename="../src/Crates/EU/CrateFirmeareUpdateEU.cpp" line="28"/>
        <source>Firmware write</source>
        <translation>Запись прошивки</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CrateFirmeareUpdateEU.cpp" line="30"/>
        <source>Firmware verify</source>
        <translation>Проверка записанной прошивки</translation>
    </message>
</context>
<context>
    <name>CratePluginCEU</name>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="22"/>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="113"/>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="143"/>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="158"/>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="164"/>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="245"/>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="256"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <source>Cannot open crate connection: </source>
        <translation type="obsolete">Не удалось установить соединение с крейтом:</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="71"/>
        <source>Choice of firmware file</source>
        <translation>Выбор файла прошивки</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="72"/>
        <source>Firmware files</source>
        <translation>Файлы прошивок</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="72"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="113"/>
        <source>Cannot switch crate to bootloader mode: </source>
        <translation>Не удалось перевезти крейт в режим загрузчика: </translation>
    </message>
    <message>
        <source>Cannot reset crate: </source>
        <translation type="obsolete">Не удалось выполнить сброс крейта:</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="23"/>
        <source>Cannot open crate connection</source>
        <translation>Не удалось установить соединение с крейтом</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="159"/>
        <source>Cannot read factory configuration</source>
        <translation>Не удалось прочитать заводские настройки крейта</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="165"/>
        <source>Cannot read user configuration</source>
        <translation>Не удалось прочитать пользовательские настройки крейта</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="246"/>
        <source>Cannot write new configuration</source>
        <translation>Не удалось записать настройки крейта</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="249"/>
        <source>Done sucessfully</source>
        <translation>Выполнено успешно</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="250"/>
        <source>Crate settings were written successfully!</source>
        <translation>Настройки крейта были успешно записаны!</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="251"/>
        <source>New settings take effect after crate reset. Reset crate yet?</source>
        <translation>Новые настройки вступят в силу после сброса крейта. Выполнить сброс сейчас?</translation>
    </message>
    <message>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="143"/>
        <location filename="../src/Crates/CEU/CratePluginCEU.cpp" line="257"/>
        <source>Cannot reset crate</source>
        <translation>Не удалось выполнить сброс крейта</translation>
    </message>
</context>
<context>
    <name>CratePluginConfigEU</name>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>Cannot open crate connection: </source>
        <translation type="obsolete">Не удалось установить соединение с крейтом:</translation>
    </message>
    <message>
        <source>Cannot open crate connection</source>
        <translation type="obsolete">Не удалось установить соединение с крейтом</translation>
    </message>
    <message>
        <source>Cannot get crate configuration: </source>
        <translation type="obsolete">Не удалось прочитать конфигурацию крейта: </translation>
    </message>
    <message>
        <source>Operation completed</source>
        <translation type="obsolete">Операция завершена</translation>
    </message>
    <message>
        <source>Crate settings were successfully written.</source>
        <translation type="obsolete">Настройки крейта были записаны успешно.</translation>
    </message>
    <message>
        <source>You must turn off and on crate for new settings to take effect!</source>
        <translation type="obsolete">Для того, чтобы они вступили в силу, необходимо отключить питание крейта и включить снова!</translation>
    </message>
    <message>
        <source>Cannot write crate settings!</source>
        <translation type="obsolete">Не удалось записать настройки крейта!</translation>
    </message>
    <message>
        <source>Choice of firmware file</source>
        <translation type="obsolete">Выбор файла прошивки</translation>
    </message>
    <message>
        <source>Firmware files</source>
        <translation type="obsolete">Файлы прошивок</translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="obsolete">Все файлы</translation>
    </message>
    <message>
        <source>Firmware Update</source>
        <translation type="obsolete">Обновление прошивки</translation>
    </message>
    <message>
        <source>Cannot reset crate</source>
        <translation type="obsolete">Не удалось выполнить сброс крейта</translation>
    </message>
    <message>
        <source>Crate firmware was successfully written.</source>
        <translation type="obsolete">Прошивка крейта была успешно записана.</translation>
    </message>
    <message>
        <source>You must turn off and on crate for run new firmware!</source>
        <translation type="obsolete">Для запуска новой прошивки необходимо отключить питание крейта и включить снова!</translation>
    </message>
    <message>
        <source>Cannot write crate firmware!</source>
        <translation type="obsolete">Не удалось записать прошивку!</translation>
    </message>
</context>
<context>
    <name>CratePluginEU</name>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="24"/>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="83"/>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="191"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="25"/>
        <source>Cannot open crate connection</source>
        <translation>Не удалось установить соединение с крейтом</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="84"/>
        <source>Cannot get crate configuration: </source>
        <translation>Не удалось прочитать конфигурацию крейта: </translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="123"/>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="218"/>
        <source>Operation completed</source>
        <translation>Операция завершена</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="124"/>
        <source>Crate settings were successfully written.</source>
        <translation>Настройки крейта были записаны успешно.</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="125"/>
        <source>You must turn off and on crate for new settings to take effect!</source>
        <translation>Для того, чтобы они вступили в силу, необходимо отключить питание крейта и включить снова!</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="127"/>
        <source>Cannot write crate settings!</source>
        <translation>Не удалось записать настройки крейта!</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="144"/>
        <source>Choice of firmware file</source>
        <translation>Выбор файла прошивки</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="145"/>
        <source>Firmware files</source>
        <translation>Файлы прошивок</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="145"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="176"/>
        <source>Firmware Update</source>
        <translation>Обновление прошивки</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="191"/>
        <source>Cannot reset crate</source>
        <translation>Не удалось выполнить сброс крейта</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="219"/>
        <source>Crate firmware was successfully written.</source>
        <translation>Прошивка крейта была успешно записана.</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="220"/>
        <source>You must turn off and on crate for run new firmware!</source>
        <translation>Для запуска новой прошивки необходимо отключить питание крейта и включить снова!</translation>
    </message>
    <message>
        <location filename="../src/Crates/EU/CratePluginEU.cpp" line="222"/>
        <source>Cannot write crate firmware!</source>
        <translation>Не удалось записать прошивку!</translation>
    </message>
</context>
<context>
    <name>CratePluginSyncCtl</name>
    <message>
        <location filename="../src/Crates/SyncCtl/CratePluginSyncCtl.cpp" line="31"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CratePluginSyncCtl.cpp" line="32"/>
        <source>Cannot open crate connection</source>
        <translation>Не удалось установить соединение с крейтом</translation>
    </message>
</context>
<context>
    <name>CrateSyncCtlDialog</name>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="75"/>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="85"/>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="94"/>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="104"/>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="113"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="76"/>
        <source>Cannot configure crate SYNC outputs</source>
        <translation>Не удалось настроить выходы разъема SYNC</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="86"/>
        <source>Cannot set START mark generation mode</source>
        <translation>Не удалось установить режим генерации метки СТАРТ</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="95"/>
        <source>Cannot generate START mark</source>
        <translation>Не удалось выполнить программную генерацию метки СТАРТ</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="105"/>
        <source>Cannot start SECOND marks generation</source>
        <translation>Не удалось запустить генерацию секундных меток</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="114"/>
        <source>Cannot stop SECOND marks generation</source>
        <translation>Не удалось остановить генерацию секундных меток</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="126"/>
        <source>%1 synchronization control</source>
        <translation>%1. Управление синхронизацией</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="144"/>
        <source>Constant low level (logical 0)</source>
        <translation>Постоянный низкий уровень (логический 0)</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="145"/>
        <source>Constant high level (logical 1)</source>
        <translation>Постоянный высокий уровень (логическая 1)</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="146"/>
        <source>Translate DIGIN1 signal state</source>
        <translation>Трансляция сигнала со входа DIGIN1</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="147"/>
        <source>Translate DIGIN2 signal state</source>
        <translation>Трансляция сигнала со входа DIGIN2</translation>
    </message>
    <message>
        <source>Translate DIGIN1 state</source>
        <translation type="obsolete"> </translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="148"/>
        <source>Translate START mark</source>
        <translation>Трансляция метки СТАРТ</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="149"/>
        <source>Translate SECOND mark</source>
        <translation>Трансляция метки СЕКУНДА</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="159"/>
        <source>Off</source>
        <translation>Отключена</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="170"/>
        <source>Internal crate timer</source>
        <translation>По внутреннему таймеру крейта</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="176"/>
        <source>On DIGIN1 input signal rise</source>
        <translation>По фронту сигнала на входе DIGIN1</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="177"/>
        <source>On DIGIN1 input signal fall</source>
        <translation>По спаду сигнала на входе DIGIN1</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="178"/>
        <source>On DIGIN2 input signal rise</source>
        <translation>По фронту сигнала на входе DIGIN2</translation>
    </message>
    <message>
        <location filename="../src/Crates/SyncCtl/CrateSyncCtlDialog.cpp" line="179"/>
        <source>On DIGIN2 input signal fall</source>
        <translation>По спаду сигнала на входе DIGIN2</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="240"/>
        <source>SYNC connector outputs</source>
        <translation>Выходы разъема SYNC</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="241"/>
        <source>Enable outputs</source>
        <translation>Разрешение выходов</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="242"/>
        <source>DIGOUT1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="243"/>
        <source>DIGOUT2</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="244"/>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="247"/>
        <source>Set</source>
        <translation>Установить</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="245"/>
        <source>START mark</source>
        <translation>Метка СТАРТ</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="246"/>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="250"/>
        <source>Mode</source>
        <translation>Режим</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="248"/>
        <source>Software generation</source>
        <translation>Программная генерация</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="249"/>
        <source>SECOND mark</source>
        <translation>Метка СЕКУНДА</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="251"/>
        <source>Start</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_CrateSyncCtlDialog.h" line="252"/>
        <source>Stop</source>
        <translation>Останов</translation>
    </message>
</context>
<context>
    <name>IpEntriesPanel</name>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="122"/>
        <source>Crate IP entries</source>
        <translation>IP-адреса крейтов</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="124"/>
        <source>Add IP address</source>
        <translation>Добавить IP-адрес</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="125"/>
        <source>Add new IP-entry</source>
        <translation>Добавить новую запись с IP-адресом крейта</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="126"/>
        <source>Remove IP address</source>
        <translation>Удалить IP-адрес</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="127"/>
        <source>Remove selected IP-entry</source>
        <translation>Удалить выбранную запись IP-адреса крейта</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="128"/>
        <source>Connect to crate</source>
        <translation>Установить соединение с крейтом</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="129"/>
        <source>Setup crate connection with address from IP-entry</source>
        <translation>Установить соединение с крейтом с IP-адресом, соответствующим выбранной записи</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="130"/>
        <source>Disconnect</source>
        <translation>Разорвать соединение</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="131"/>
        <source>Close crate connection with address form IP-entry</source>
        <translation>Разорвать соединение с крейтом с адресом, соответствующим выбранной записи</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="132"/>
        <source>Autoconnect with crates</source>
        <translation>Автосоединение с крейтами</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="133"/>
        <source>Connect to all crates with address from IP-entries marked &apos;auto&apos;</source>
        <translation>Установить соединение со всеми крейтами, помеченными для автоподключения при старте</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="134"/>
        <source>Disconnect all crates</source>
        <translation>Разорвать соединение со всеми крейтами</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/IpEntriesPanel.cpp" line="135"/>
        <source>Close all opened TCP/IP crate connections</source>
        <translation>Разорвать соединения со всеми крейтами, кодключенными по TCP/IP</translation>
    </message>
</context>
<context>
    <name>LQTableCopyAction</name>
    <message>
        <location filename="../src/LQTableWidget/LQTableCopyAction.cpp" line="65"/>
        <source>&amp;Copy</source>
        <translation>&amp;Копировать</translation>
    </message>
    <message>
        <location filename="../src/LQTableWidget/LQTableCopyAction.cpp" line="66"/>
        <source>Copy selected text</source>
        <translation>Копировать выделенный текст</translation>
    </message>
</context>
<context>
    <name>LtrCratesModel</name>
    <message>
        <location filename="../src/LtrCratesModel.cpp" line="160"/>
        <source>Initilization</source>
        <translation>Инициализация</translation>
    </message>
    <message>
        <location filename="../src/LtrCratesModel.cpp" line="164"/>
        <source>Invalid module ID (!)</source>
        <translation>Неверный ID (!)</translation>
    </message>
    <message>
        <source>Invalid (!)</source>
        <translation type="vanished">Ошибка (!)</translation>
    </message>
</context>
<context>
    <name>LtrIpEntriesModel</name>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="63"/>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="74"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="63"/>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="74"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="94"/>
        <source>Connecting...</source>
        <translation>Подключение...</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="98"/>
        <source>Offline</source>
        <translation>Отключен</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="102"/>
        <source>Online</source>
        <translation>Подключен</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="106"/>
        <source>Error!</source>
        <translation>Ошибка!</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="142"/>
        <source>A</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="162"/>
        <source>Autoconnection on ltrd startup</source>
        <translation>Автоматическое подключение при запуске ltrd</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="165"/>
        <source>Reconnection on error</source>
        <translation>Повторное подключение при ошибке</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="168"/>
        <source>Crate IP-address</source>
        <translation>IP-адрес крейта</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="171"/>
        <source>Connection status</source>
        <translation>Состояние подключения</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="174"/>
        <source>Crate serial number</source>
        <translation>Серийный номер крейта</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="obsolete">Авто</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="145"/>
        <source>R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="148"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="151"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../src/IpEntriesPanel/LtrIpEntriesModel.cpp" line="154"/>
        <source>Serial</source>
        <translation>Серийный номер</translation>
    </message>
</context>
<context>
    <name>LtrManagerEnv</name>
    <message>
        <location filename="../src/LtrManagerEnv.cpp" line="59"/>
        <source>Crate settings...</source>
        <translation>Настройки крейта...</translation>
    </message>
    <message>
        <location filename="../src/LtrManagerEnv.cpp" line="60"/>
        <source>Crate sync control...</source>
        <translation>Управление синхронизацией крейта...</translation>
    </message>
    <message>
        <location filename="../src/LtrManagerEnv.cpp" line="61"/>
        <source>Crate reset</source>
        <translation>Сброс крейта</translation>
    </message>
    <message>
        <location filename="../src/LtrManagerEnv.cpp" line="62"/>
        <source>Update crate firmware...</source>
        <translation>Обновление прошивки крейта...</translation>
    </message>
</context>
<context>
    <name>LtrdConnectionSettings</name>
    <message>
        <location filename="../src/LtrdConnectionSettings/LtrdConnectionSettings.cpp" line="12"/>
        <source>%1 settings</source>
        <translation>Настройки %1</translation>
    </message>
    <message>
        <location filename="../src/LtrdConnectionSettings/LtrdConnectionSettings.cpp" line="67"/>
        <source>Get settings error!</source>
        <translation>Ошибка получения настроек!</translation>
    </message>
    <message>
        <location filename="../src/LtrdConnectionSettings/LtrdConnectionSettings.cpp" line="68"/>
        <source>Cannot get ltrd settings</source>
        <translation>Не удалось получить настройки ltrd</translation>
    </message>
    <message>
        <location filename="../src/LtrdConnectionSettings/LtrdConnectionSettings.cpp" line="103"/>
        <source>Cannot set ltrd settings</source>
        <translation>Не удалось установить настройки ltrd</translation>
    </message>
    <message>
        <source>Cannot get ltrd settings. Error %1</source>
        <translation type="obsolete">Не удалось получить настройки ltrd. Ошибка %1</translation>
    </message>
    <message>
        <location filename="../src/LtrdConnectionSettings/LtrdConnectionSettings.cpp" line="102"/>
        <source>Set settings error!</source>
        <translation>Ошибка записи настроек!</translation>
    </message>
    <message>
        <source>Cannot set ltrd settings. Error %1</source>
        <translation type="obsolete">Не удалось установить настройки ltrd. Ошибка %1</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="200"/>
        <source>Log level</source>
        <translation>Уровень журнала</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="201"/>
        <source>Ethernet (TCP/IP) settings</source>
        <translation>Настройки Ethernet (TCP/IP)</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="202"/>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="206"/>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="207"/>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="209"/>
        <source> ms</source>
        <translation> мс</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="203"/>
        <source>Crate polling time</source>
        <translation>Время опроса крейтов</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="204"/>
        <source>Commands timeout</source>
        <translation>Таймаут на выполнение команд</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="205"/>
        <source>Connection timeout</source>
        <translation>Таймаут на установление соединения</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="208"/>
        <source>Reconnection time</source>
        <translation>Время для повторного подключения</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="211"/>
        <source>TCP send no delay mode</source>
        <translation>Передача по TCP без задержки</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="212"/>
        <source>Crate data buffers</source>
        <translation>Буферы данных крейта</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="213"/>
        <source>Module send buffer size</source>
        <translation>Размер буфера модуля на передачу</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="214"/>
        <source>Module receive buffer size</source>
        <translation>Размер буфера модуля на прием</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="215"/>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_LtrdConnectionSettings.h" line="216"/>
        <source> KWords</source>
        <translation> КСлов</translation>
    </message>
</context>
<context>
    <name>LtrdLogModel</name>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="62"/>
        <source>Record time</source>
        <translation>Время записи</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="65"/>
        <source>Level</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="68"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="94"/>
        <source>Critical Errors</source>
        <translation>Крит. ошибки</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="95"/>
        <source>Errors</source>
        <translation>Ошибки</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="96"/>
        <source>Warnings</source>
        <translation>Предупреждения</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="97"/>
        <source>Info</source>
        <translation>Информация</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="98"/>
        <source>Detail</source>
        <translation>Детали</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="99"/>
        <source>Debug 1</source>
        <translation>Отладка 1</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="100"/>
        <source>Debug 2</source>
        <translation>Отладка 2</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogModel.cpp" line="101"/>
        <source>Debug 3</source>
        <translation>Отладка 3</translation>
    </message>
</context>
<context>
    <name>LtrdLogPanel</name>
    <message>
        <location filename="../src/Log/LtrdLogPanel.cpp" line="63"/>
        <source>Ctrl+D</source>
        <translation>Ctrl+D</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogPanel.cpp" line="201"/>
        <source>Select &amp;All</source>
        <translation>В&amp;ыделить всё</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogPanel.cpp" line="198"/>
        <source>%1 log</source>
        <translation>Журнал %1</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogPanel.cpp" line="199"/>
        <source>Show log level</source>
        <translation>Уровень отображения журнала</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogPanel.cpp" line="200"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../src/Log/LtrdLogPanel.cpp" line="202"/>
        <source>Recalculate rows size</source>
        <translation>Пересчитать размер строк</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/MainWindow.cpp" line="233"/>
        <source>Quit from %1</source>
        <translation>Выйти из программы %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="234"/>
        <source>Reset selected %1 module</source>
        <translation>Выполнить сброс выбранного модуля %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="236"/>
        <source>Connect to %1</source>
        <translation>Установить соединение с %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="237"/>
        <source>Disconnect from %1</source>
        <translation>Разорвать соединение с %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="238"/>
        <source>Add new %1 connection</source>
        <translation>Добавить новое соединение с %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="239"/>
        <source>Delete %1 connection</source>
        <translation>Удалить соединение с %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="240"/>
        <source>%1 settings...</source>
        <translation>Настройки %1...</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="241"/>
        <source>Change %1 settings</source>
        <translation>Изменить настройки %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="242"/>
        <source>%1 log</source>
        <translation>Журнал %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="243"/>
        <source>Show %1 log panel</source>
        <translation>Показать панель с журналом %1</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="278"/>
        <source>Crate connected</source>
        <translation>Крейт подключен</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="278"/>
        <source>Connected crate: </source>
        <translation>Подключен крейт: </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="282"/>
        <source>Crate disconnected</source>
        <translation>Крейт отключен</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="282"/>
        <source>Disconnected crate: </source>
        <translation>Отключенный крейт: </translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="309"/>
        <source>New ltrd connection</source>
        <translation>Новое соединение с ltrd</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="312"/>
        <source>ltrd connection closed by error!</source>
        <translation>Соединение с ltrd разорвано из-за ошибки!</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="419"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="419"/>
        <source>There is opened client connection with this module.</source>
        <translation>С этим модулем установлено клиентское соединение.</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="420"/>
        <source>If you reset module this connection will be closed!</source>
        <translation>Если выполнить его сброс, то это соединение будут разорвано!</translation>
    </message>
    <message>
        <location filename="../src/MainWindow.cpp" line="421"/>
        <source>You still want to reset this module?</source>
        <translation>Все равно выполнить сброс данного модуля?</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="240"/>
        <source>Quit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="241"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="242"/>
        <source>IP addresses</source>
        <translation>IP-адреса</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="244"/>
        <source>Show panel with crates IP address entries</source>
        <translation>Показать панель с IP-адресами крейтов</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="246"/>
        <source>Ctrl+I</source>
        <translation>Ctrl+I</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="247"/>
        <source>Statistic</source>
        <translation>Статистика</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="249"/>
        <source>Show statistic panel</source>
        <translation>Показать панель со статистикой</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="251"/>
        <source>Module reset</source>
        <translation>Сброс модуля</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="252"/>
        <source>Autoconnect at startup</source>
        <translation>Подключить при запуске</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="253"/>
        <source>Show/hide main window</source>
        <translation>Показать/скрыть основное окно</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="254"/>
        <source>System</source>
        <translation>Системный</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="256"/>
        <source>Set system language</source>
        <translation>Установить язык интерфейса, соответствующий языку системы</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="258"/>
        <source>Russian</source>
        <translation>Русский</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="260"/>
        <source>Set Russian language</source>
        <translation>Установить русский язык интерфейса</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="262"/>
        <source>English</source>
        <translation>Английский</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="264"/>
        <source>Set English language</source>
        <translation>Установить английский язык интерфейса</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="266"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="267"/>
        <source>Module</source>
        <translation>Модуль</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="268"/>
        <source>Windows</source>
        <translation>Окна</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="269"/>
        <source>Crate</source>
        <translation>Крейт</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="270"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../../../../_builds/ltr_cross_sdk/release/ltrmanager/ui_MainWindow.h" line="271"/>
        <source>Toolbar</source>
        <translation>Панель задач</translation>
    </message>
</context>
<context>
    <name>StatisticModel</name>
    <message>
        <location filename="../src/StatisticPanel/StatisticModel.cpp" line="102"/>
        <source>%1 connection</source>
        <translation>Соединение с %1</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/StatisticModel.cpp" line="130"/>
        <source>Crate parameters</source>
        <translation>Параметры крейта</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/StatisticModel.cpp" line="157"/>
        <source>Slot %1 paramters</source>
        <translation>Параметры слота %1</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/StatisticModel.cpp" line="199"/>
        <source>Parameter</source>
        <translation>Параметр</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/StatisticModel.cpp" line="201"/>
        <source>Value</source>
        <translation>Значение</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="64"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="309"/>
        <source>State</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="70"/>
        <source>Online</source>
        <translation>Подключен</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="73"/>
        <source>Connecting...</source>
        <translation>Подключение...</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="76"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="79"/>
        <source>Offline</source>
        <translation>Отключен</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="82"/>
        <source>Unknown state</source>
        <translation>Неизвестное состояние</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="111"/>
        <source>Crate type</source>
        <translation>Тип крейта</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="117"/>
        <source>Crate serial</source>
        <translation>Серийный номер крейта</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="123"/>
        <source>Interface</source>
        <translation>Интерфейс</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="124"/>
        <source>Interface used for this connection with crate</source>
        <translation>Интерфейс, по которому установлено соединение с крейтом</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="128"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="139"/>
        <source>Unknown</source>
        <translation>Неизвестный</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="133"/>
        <source>Mode</source>
        <translation>Режим работы</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="134"/>
        <source>Current crate mode (work/bootloader/configure)</source>
        <translation>Текущий режим работы крейта (рабочий/загрузчик/настройка)</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="136"/>
        <source>Work</source>
        <translation>Рабочий</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="137"/>
        <source>Bootloader</source>
        <translation>Загрузчик</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="138"/>
        <source>Configure only</source>
        <translation>Только настройка</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="148"/>
        <source>Firmware version</source>
        <translation>Версия прошивки</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="149"/>
        <source>Crate MCU or DSP firmware version</source>
        <translation>Версия прошивки контроллера или DSP крейта</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="162"/>
        <source>FPGA version</source>
        <translation>Версия прошивки ПЛИС</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="163"/>
        <source>Crate FPGA firmware version</source>
        <translation>Версия прошивки ПЛИС крейта</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="169"/>
        <source>Revision</source>
        <translation>Ревизия</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="170"/>
        <source>Crate board revision</source>
        <translation>Ревизия платы крейта</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="177"/>
        <source>Protocol version</source>
        <translation>Версия протокола</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="186"/>
        <source>Slot config version</source>
        <translation>Версия конф. слотов</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="187"/>
        <source>Version of protocol for saving slot configuration (if implemented)</source>
        <translation>Версия протокола для сохранения конфигурации слотов (если реализован)</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="197"/>
        <source>Connection time</source>
        <translation>Время подключения</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="58"/>
        <source>%1 version</source>
        <translation>Версия %1</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="59"/>
        <source>Version of selected %1 instance</source>
        <translation>Версия экземпляра %1, с которым установлено соединение</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="65"/>
        <source>State of connection with %1</source>
        <translation>Состояние соединения с %1</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="155"/>
        <source>Bootloader version</source>
        <translation>Версия загрузчика</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="156"/>
        <source>Crate MCU or DSP bootloader version</source>
        <translation>Версия загрузчика контроллера или сигнального процессора крейта</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="178"/>
        <source>Version of protocol for crate and %1 communication</source>
        <translation>Версия протокола обмена между крейтом и %1</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="198"/>
        <source>Time when was established %1 connection with this crate</source>
        <translation>Время, когда было установлено соединение %1 с данным крейтом</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="206"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="342"/>
        <source>Clients</source>
        <translation>Всего клиентов</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="207"/>
        <source>Total client connections number with crate and its modules</source>
        <translation>Общее количество клиентский соединений с крейтом и его модулями</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="215"/>
        <source>Control connections</source>
        <translation>Управляющих соединений</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="216"/>
        <source>Control client connections number with this crate</source>
        <translation>Общее количество управляющих клиентских соединений с данным крейтом</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="225"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="351"/>
        <source>Sent words</source>
        <translation>Передано слов</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="226"/>
        <source>Number of words sent to crate and its modules</source>
        <translation>Количество слов, переданных в крейт и его модули</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="233"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="359"/>
        <source>Received words</source>
        <translation>Принято слов</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="234"/>
        <source>Number of words received from crate and its modules</source>
        <translation>Количество слов, принятых от крейта и его модулей</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="241"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="376"/>
        <source>Send rate</source>
        <translation>Скорость передачи</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="242"/>
        <source>Total send rate of words to crate and its modules</source>
        <translation>Общая скорость передачи слов в крейт и его модули</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="244"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="253"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="370"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="379"/>
        <source>words/s</source>
        <translation>слов/с</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="250"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="367"/>
        <source>Receive rate</source>
        <translation>Скорость приема</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="251"/>
        <source>Total receive rate of words from crate and its modules</source>
        <translation>Общая скорость приема слов из крейта и его модулей</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="260"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="441"/>
        <source>&apos;Start&apos; marks</source>
        <translation>Метки &apos;Старт&apos;</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="261"/>
        <source>Total number of &apos;Start&apos; marks received from crate and its modules</source>
        <translation>Общее количество меток &apos;Старт&apos;, принятых от крейта и его модулей</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="269"/>
        <location filename="../src/StatisticPanel/statistic_params.h" line="453"/>
        <source>&apos;Second&apos; marks</source>
        <translation>Метки &apos;Секунда&apos;</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="270"/>
        <source>Total number of &apos;Second&apos; marks received from crate and its modules</source>
        <translation>Общее количество меток &apos;Секунда&apos;, принятых от крейта и его модулей</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="278"/>
        <source>Extended time mark</source>
        <translation>Расширенная метка времени</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="279"/>
        <source>Value of most recently received time mark with extended format (if any)</source>
        <translation>Последнее значение принятой от крейта метки времени расширенного формата</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="286"/>
        <source>Thermometer (bottom)</source>
        <translation>Термометр (нижний)</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="287"/>
        <source>Temperature value from bottom thermometer</source>
        <translation>Значение температуры нижнего термометра</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="293"/>
        <source>Thermometer (top)</source>
        <translation>Термометр (верхний)</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="294"/>
        <source>Temperature value from top thermometer</source>
        <translation>Значение температуры верхнего термометра</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="310"/>
        <source>Selected slot state</source>
        <translation>Состояние выбранного слота</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="322"/>
        <source>Empty slot</source>
        <translation>Пустой слот</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="324"/>
        <source>Module ID identifying...</source>
        <translation>Определение ID модуля...</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="326"/>
        <source>Invalid module ID</source>
        <translation>Неверный идентификатор модуля</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="327"/>
        <source>Installed module</source>
        <translation>Установлен модуль</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="333"/>
        <source>Interface speed mode</source>
        <translation>Режим скорости интерфейса</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="334"/>
        <source>Slot interface speed mode to send words from crate to module</source>
        <translation>Режим скорости данного слота для передачи слов из крейта в модуль</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="336"/>
        <source>High</source>
        <translation>Высокая</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="336"/>
        <source>Low</source>
        <translation>Низкая</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="343"/>
        <source>Client connections number with this module</source>
        <translation>Количество клиентов, установивших соединение с данным модулем</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="352"/>
        <source>Number of words sent to this module</source>
        <translation>Количество слов, переданных в модуль</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="360"/>
        <source>Number of words received from this module</source>
        <translation>Количество слов, принятых из модуля</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="368"/>
        <source>Receive rate of words from this module</source>
        <translation>Текущая скорость приема слов из данного модуля</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="377"/>
        <source>Send rate of words from this module</source>
        <translation>Текущая скорость передачи слов в данный модуль</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="386"/>
        <source>Receive buffer</source>
        <translation>Буфер на прием</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="387"/>
        <source>Current percentage of %1 receive buffer utilizatione</source>
        <translation>Текущий процент заполненности буфера %1 на прием</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="395"/>
        <source>Current percentage of %1 send buffer utilization</source>
        <translation>Текущий процент заполненности буфера %1 на передачу</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="402"/>
        <source>Maximum percentage of %1 receive buffer utilizatione</source>
        <translation>Максимальный процент заполненности буфера %1 на прием</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="410"/>
        <source>Maximum percentage of %1 send buffer utilizatione</source>
        <translation>Максимальный процент заполненности буфера %1 на передачу</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="417"/>
        <source>Total number of %1 receive buffer overflows</source>
        <translation>Общее количество переполнений буфера %1 на прием</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="394"/>
        <source>Send buffer</source>
        <translation>Буфер на передачу</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="401"/>
        <source>Max. recieve buffer</source>
        <translation>Макс. буфер на прием</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="409"/>
        <source>Max. send buffer</source>
        <translation>Макс. буфер на передачу</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="416"/>
        <source>Buffer overflows</source>
        <translation>Переполнений буфера</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="427"/>
        <source>Dropped words</source>
        <translation>Потеряно слов</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="428"/>
        <source>Total number of dropped words for %1 receive buffer overflows</source>
        <translation>Общее количество потерянных слов из-за переполнения буфера %1 на прием</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="442"/>
        <source>Number of &apos;Start&apos; marks received from this modules</source>
        <translation>Количество меток &apos;Старт&apos;, принятых от данного модуля</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="454"/>
        <source>Number of &apos;Second&apos; marks received from this modules</source>
        <translation>Количество меток &apos;Секунда&apos;, принятых от данного модуля</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="464"/>
        <source>Hard fifo: size</source>
        <translation>Ап. очередь: размер</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="465"/>
        <source>Size (in words) of internal module hardware output fifo</source>
        <translation>Размер (в словах) внутренней аппаратной очереди модуля на вывод</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="472"/>
        <source>Hard fifo: unack. words</source>
        <translation>Ап. очередь: неподтв. слов</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="473"/>
        <source>Number of words sent to module but not acknowledged</source>
        <translation>Количество слов,  переданных в модуль, но не подтвержденных</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="480"/>
        <source>Hard fifo: starvations</source>
        <translation>Ап. очередь: голодания</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="481"/>
        <source>How many times hardware fifo &apos;starvation&apos; event (empty fifo when required data) occures since last reset</source>
        <translation>Сколько раз возникало событие &apos;голодание&apos; (пустая очередь, когда требуются данный) аппаратной очереди модуля после последнего сброса</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="491"/>
        <source>Hard fifo: overflows</source>
        <translation>Ап. очередь: переполнения</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="492"/>
        <source>How many times hardware fifo overflows occures since last reset</source>
        <translation>Сколько раз возникало переполнение аппаратной очереди модуля после последнего сброса</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="503"/>
        <source>Hard fifo: state</source>
        <translation>Ап. очередь: состояние</translation>
    </message>
    <message>
        <location filename="../src/StatisticPanel/statistic_params.h" line="504"/>
        <source>Internal state of hardware output fifo showing its utilization at the crurrent moment</source>
        <translation>Внутреннее состояние аппаратной очереди модуля, характеризуящее ее заполненность на текущей момент</translation>
    </message>
</context>
<context>
    <name>StatisticPanel</name>
    <message>
        <location filename="../src/StatisticPanel/StatisticPanel.cpp" line="48"/>
        <source>Statistic</source>
        <translation>Статистика</translation>
    </message>
</context>
</TS>
