#ifndef QLTRCRATESMODEL_H
#define QLTRCRATESMODEL_H

#include <QAbstractItemModel>
#include <QList>
#include <QList>
#include <QSystemTrayIcon>


#include "LtrdConnection.h"

class SrvConItem;
class TreeItem;

/** Древовоидная модель для представления информации о доступных ltrd-серверах,
    крейтах и модулях.
    Элементы верхнего уровня соответствуют соединениям с серверами,
    второго уровня - активным крейтам для этого сервера,
    третьего уровня - id модулей в слотах крейта */
class LtrCratesModel : public QAbstractItemModel {
    Q_OBJECT
public:
    explicit LtrCratesModel(QObject *parent = 0);
    ~LtrCratesModel();

    /* добавляем новое подключение к демону ltrd */
    void addNewSrvCon(LtrdConnection *con);
    /* удаляем существующее соединение */
    void removeSrvCon(LtrdConnection *con);

    int cratesCnt(void) const;

    /* получение по индексу информацию о соответствующем индексу соединении,
       крейту и слоту */
    void getInfo(QModelIndex ind, LtrdConnection **con, LtrCrateInfo **info=NULL,
                 int* slot=NULL) const;

    /* стандартный интерфейс для модели */
    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;


    /* получить список всех добавленных соединений */
    QList<LtrdConnection*> connectionList() const;
    
signals:
    
public slots:

private:
    SrvConItem* foundSrvConItem(LtrdConnection* con, int* row);
    TreeItem* m_root;



private slots:
    void ltrdStatusChanged(LtrdConnection::connStatus status);
    void crateInfoChanged(LtrCrateInfo* info, LtrConEvent event);
};

#endif // QLTRCRATESMODEL_H
