#ifndef ACTIONPLUGIN_H
#define ACTIONPLUGIN_H

#include "LtrManagerEnv.h"

class ActionPlugin  : public QObject {
    Q_OBJECT
public:
    ActionPlugin(LtrManagerEnv *env) : QObject(env) {}
    virtual bool actionEnabled(LtrManagerEnv *env, LtrManagerEnv::ActionType type) = 0;
    virtual void actionActivate(LtrManagerEnv *env, LtrManagerEnv::ActionType type) = 0;
};

#endif // ACTIONPLUGIN_H

