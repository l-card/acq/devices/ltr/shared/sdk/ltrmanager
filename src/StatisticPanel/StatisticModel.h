#ifndef STATISTICMODEL_H
#define STATISTICMODEL_H

#include <QAbstractItemModel>
#include <QList>
#include "LtrdConnection.h"


class LtrCrateParam;
class LtrSrvConParam;
class LtrSlotParam;

/* Класс, являющийся интерфейсом для отображения статистики.
   Отображение выполняется в виде дерева, на первом уровне - до трех узлов
   (1-параметры соединения с сервером, 2 - параметры крейта, 3 - модуля),
   на втором уровне - 2 столбца параметр/значение.

   Выбранное соединение, слот и модуль устанавливаются с помощью setItem(),
   класс сам отслеживает, изменились ли эти параметры с предыдущего раза.
   Класс сам отслеживает, когда изменяются параметры соединения/крейта/модуля,
   соединяя свои слоты с сигналомы LtrSrvConnection, т.е. действия извне нужны
   только при смене выделенного соедининия/крейта/модуля */
class StatisticModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    explicit StatisticModel(QObject *parent = 0);
    ~StatisticModel();

    static const int invalidSlot = -1;


    QModelIndex index(int row, int column, const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &index) const;
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData (int section, Qt::Orientation orientation,
                         int role = Qt::DisplayRole ) const;
    QSize span ( const QModelIndex & index ) const;
signals:
    
public slots:

    void setItem(LtrdConnection* con, LtrCrateInfo* crate, int slot);
    void srvConStatusChanged();
    void crateSelectChanged();
    void slotSelectChanged();
    void crateStateChanged(LtrCrateInfo *crate, LtrConEvent evt);



private:

    int getSlotParamCnt(const TLTR_MODULE_STATISTIC *stat) const;
    void slotParamsChanged();

    LtrdConnection *m_con;
    LtrCrateInfo* m_crate;
    TLTR_CRATE_STATISTIC m_stat;
    TLTR_MODULE_STATISTIC m_mstat;
    int m_slot;

    QList<LtrSrvConParam *> m_srvParams;
    QList<LtrCrateParam *>  m_crateParams;
    QList<LtrSlotParam *> m_slotParams;



    static const int group_id_con = 1;
    static const int group_id_crate = 2;
    static const int group_id_slot = 3;


};

#endif // STATISTICMODEL_H
