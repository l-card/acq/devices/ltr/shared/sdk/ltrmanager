#ifndef LTRCEU_DEFS_H
#define LTRCEU_DEFS_H

#include "ltr/include/lwintypes.h"

#define IOADDR_SYS_COMMAND      0x90005000
#define IOADDR_CONF_FACT        0x90005100
#define IOADDR_CONF_USER        0x90005200

#define CONF_FACT_SIG           0xEEDA0001
#define CONF_USER_SIG           0xEEDB0001

/* Команды (пишутся в IOADDR_SYS_COMMAND) */
#define LTR021M_CMD_REBOOT      0x5AFEC0DE
#define LTR021M_CMD_GO_BOOTLDR  0xB00710AD



#define CONF_SERIAL_SIZE        16
#define CONF_PASSWD_SIZE        16
#define CONF_MAC_SIZE           6
#define CONF_IP_SIZE            4


/* Битовые флаги для опций интерфейса CONF_IFACE_NET */
#define CONF_NETOPT_USERMAC     0x01    /* 1 = использовать user mac_addr, 0 = factory mac_addr */
#define CONF_NETOPT_DHCP        0x02    /* 1 = получать адрес через DHCP, 0 = static IP */
#define CONF_NETOPT_NODELAY_CMD 0x10    /* отключить Nagle Algorithm для командного сокета */
#define CONF_NETOPT_NODELAY_DTA 0x20    /* отключить Nagle Algorithm для сокета данных */
#define CONF_NETOPT_NODELAY_ALL (CONF_NETOPT_NODELAY_CMD | CONF_NETOPT_NODELAY_DTA)

typedef struct {
    DWORD signature;
    DWORD size;
    char passwd[CONF_PASSWD_SIZE];
    char serial[CONF_SERIAL_SIZE];
    BYTE mac_addr[CONF_MAC_SIZE];
    BYTE reserved[2];
} ltr021m_conf_fact_t;

typedef struct {
    DWORD signature;
    DWORD size;
    char passwd[CONF_PASSWD_SIZE];
    BYTE iface;                      /* Выбор интерфейса (CONF_IFACE_...) */
    BYTE net_options;                /* Опции сетевого интерфейса */
    BYTE mac_addr[CONF_MAC_SIZE];    /* используется, если в net_options USERMAC == 1 */
    BYTE ip_addr[CONF_IP_SIZE];
    BYTE ip_netmask[CONF_IP_SIZE];
    BYTE ip_gateway[CONF_IP_SIZE];
} ltr021m_conf_user_t;


#endif // LTRCEU_DEFS_H

