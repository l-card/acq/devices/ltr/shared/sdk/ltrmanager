#ifndef IPENTRIESPANEL_H
#define IPENTRIESPANEL_H

#include <QDockWidget>
#include <QModelIndex>
#include "LtrdConnection.h"

class QTableView;
class QAction;
class LtrIpEntriesModel;
class QMenu;


/** Боковая панель, предназначенная для изменения записей IP-адресов
    крейтов. Содержит таблицу с текущими адресами и toolbar с кнопками
    для манипуляции с записями */
class IpEntriesPanel : public QDockWidget {
    Q_OBJECT
public:
    explicit IpEntriesPanel(QWidget *parent = 0);

    QTableView *view() const {return m_ipTable;}
    
signals:
    void reqAddIp(qint32 addr, qint32 flags, bool perm);
    void reqRemIp(qint32 addr, bool perm);
    void reqFlagChange(qint32 addr, qint32 flags, bool perm);
    void reqConnect(qint32 addr);
    void reqDisconnect(qint32 addr);
    void reqConnectAuto();
    void reqDisconnectAll();

public slots:
    /** передача активного соединения с сервером, список адресов которого
        должен быть отображен */
    void setConnection(LtrdConnection *con);


protected:
    void closeEvent ( QCloseEvent * event);
    void changeEvent ( QEvent * event );
private:
    LtrIpEntriesModel *m_model;
    QTableView *m_ipTable;
    QAction *m_actAdd, *m_actRemove, *m_actConnect, *m_actDisconnect,
            *m_actConnectAuto, *m_actDisconnectAll;
    LtrdConnection *m_con;
    QWidget *mainWgt;
    QMenu *m_contextMenu;
private slots:
    void addIpEntry();
    void remIpEntry();
    void ipConnect();
    void ipDisconnect();
    void ipConnoctAuto();
    void ipDisconnectAll();
    void selectionChanged(QModelIndex current, QModelIndex old);
    void selectionClear(void);
    void ipEntryDataChanged(QModelIndex ind);
    void ipDoubleClicked(QModelIndex ind);
    /* проверка разрешены ли действия автоподключения и дисконнекта всех */
    void checkActEnable();
    void srvStatusChanged(LtrdConnection::connStatus);

    void retranslateUi(void);
};

#endif // IPENTRIESPANEL_H
