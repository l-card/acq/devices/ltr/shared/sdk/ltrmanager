#include "LtrdConnectionSettings.h"
#include "ui_LtrdConnectionSettings.h"
#include "LtrdConnection.h"
#include <QMessageBox>

LtrdConnectionSettings::LtrdConnectionSettings(LtrdConnection *con, QStringList logLvlNames, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LtrdConnectionSettings),
    m_con(con) {

    ui->setupUi(this);
    setWindowTitle(tr("%1 settings").arg(LTR_SERVICE_NAME));

    int err = 0;
    unsigned val = 0;


    ui->logLvlBox->addItems(logLvlNames);

    err = con->getLogLevel(&val);
    if (!err)
        ui->logLvlBox->setCurrentIndex(static_cast<int>(val));
    if (!err)
        err = con->getUintParam(LTRD_PARAM_ETH_CRATE_POLL_TIME, &val);
    if (!err)
        ui->ethCratePollTime->setValue(static_cast<int>(val));
    if (!err)
        err = con->getUintParam(LTRD_PARAM_ETH_CRATE_CTLCMD_TOUT, &val);
    if (!err)
        ui->ethCrateCtlTout->setValue(static_cast<int>(val));
    if (!err)
        err = con->getUintParam(LTRD_PARAM_ETH_CRATE_CON_TOUT, &val);
    if (!err)
        ui->ethCrateConTout->setValue(static_cast<int>(val));

    if (!err && con->supportReconnection()) {
        err = con->getUintParam(LTRD_PARAM_ETH_CRATE_RECONNECT_TIME, &val);
        if (!err)
            ui->ethCrateReconTime->setValue(static_cast<int>(val));
    } else {
        ui->ethCrateReconTime->setEnabled(false);
    }

    if (!err && con->supportModuleBufCfg()) {
        err = con->getUintParam(LTRD_PARAM_MODULE_SEND_BUF_SIZE, &val);
        if (!err)
            ui->moduleSndBufSize->setValue(static_cast<int>(val/1024));

        err = con->getUintParam(LTRD_PARAM_MODULE_RECV_BUF_SIZE, &val);
        if (!err)
            ui->moduleRcvBufSize->setValue(static_cast<int>(val/1024));
    } else {
        ui->moduleSndBufSize->setEnabled(false);
        ui->moduleRcvBufSize->setEnabled(false);
    }

    if (!err && con->supportTCPNoDelayCfg()) {
        err = con->getUintParam(LTRD_PARAM_ETH_SEND_NODELAY, &val);
        if (!err) {
            ui->ethNoDelay->setChecked(val != 0);
        }
    } else {
        ui->ethNoDelay->setEnabled(false);
    }

    if (err) {
        QMessageBox::critical(this, tr("Get settings error!"),
                              QString("%1: %2").arg(tr("Cannot get ltrd settings"))
                              .arg(con->getErrorString(err)),
                              QMessageBox::Ok, QMessageBox::Ok);
    }
}

LtrdConnectionSettings::~LtrdConnectionSettings() {
    delete ui;
}

void LtrdConnectionSettings::accept() {
    int err = 0;
    err = m_con->setLogLevel(static_cast<unsigned>(ui->logLvlBox->currentIndex()));
    if (!err)
        err = m_con->setUintParam(LTRD_PARAM_ETH_CRATE_POLL_TIME, static_cast<unsigned>(ui->ethCratePollTime->value()));
    if (!err)
        err = m_con->setUintParam(LTRD_PARAM_ETH_CRATE_CTLCMD_TOUT, static_cast<unsigned>(ui->ethCrateCtlTout->value()));
    if (!err)
        err = m_con->setUintParam(LTRD_PARAM_ETH_CRATE_CON_TOUT, static_cast<unsigned>(ui->ethCrateConTout->value()));
    if (!err && m_con->supportReconnection())
        err = m_con->setUintParam(LTRD_PARAM_ETH_CRATE_RECONNECT_TIME, static_cast<unsigned>(ui->ethCrateReconTime->value()));
    if (!err && m_con->supportModuleBufCfg()) {
        err = m_con->setUintParam(LTRD_PARAM_MODULE_SEND_BUF_SIZE, static_cast<unsigned>(ui->moduleSndBufSize->value()) * 1024);
        if (!err) {
            err = m_con->setUintParam(LTRD_PARAM_MODULE_RECV_BUF_SIZE, static_cast<unsigned>(ui->moduleRcvBufSize->value()) * 1024);
        }
    }
    if (!err && m_con->supportTCPNoDelayCfg()) {
         err = m_con->setUintParam(LTRD_PARAM_ETH_SEND_NODELAY, ui->ethNoDelay->isChecked() ? 1 : 0);
    }

    if (!err) {
        QDialog::accept();
    } else {
        QMessageBox::critical(this, tr("Set settings error!"),
                              QString("%1: %2").arg(tr("Cannot set ltrd settings"))
                              .arg(m_con->getErrorString(err)),
                              QMessageBox::Ok, QMessageBox::Ok);
    }
}
