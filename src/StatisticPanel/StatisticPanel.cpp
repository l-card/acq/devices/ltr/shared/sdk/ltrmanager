#include "StatisticPanel.h"
#include "StatisticModel.h"

#include <QTreeView>
#include <QEvent>


StatisticPanel::StatisticPanel(QWidget *parent) :
    QDockWidget(parent) {
    this->setObjectName("statisticPanel");   
    m_view = new QTreeView();
    m_model = new StatisticModel();
    m_view->setModel(m_model);
    m_view->setFirstColumnSpanned(0, QModelIndex(), true);
    m_view->setFirstColumnSpanned(1, QModelIndex(), true);
    m_view->setFirstColumnSpanned(2, QModelIndex(), true);
    m_view->setColumnWidth(0, 200);
    /*m_view->setStyleSheet("QTreeView::item {"
                          "border: 1px solid #d9d9d9;"
                          "border-top-color: transparent;"
                          "border-bottom-color: transparent;"
                          "}");*/
    //m_view->setSelectionMode(QAbstractItemView::MultiSelection);


    this->setWidget(m_view);


    connect(m_model, SIGNAL(rowsInserted( const QModelIndex &, int , int )),
            m_view, SLOT(expandAll()));//SLOT(expand(QModelIndex)));
}

void StatisticPanel::setItem(LtrdConnection *con, LtrCrateInfo *crate, int slot) {
    m_model->setItem(con, crate, slot);
}

void StatisticPanel::changeEvent(QEvent *event) {
    switch (event->type()) {
        case QEvent::LanguageChange:
            retranslateUi();
            break;
    }

    QDockWidget::changeEvent(event);
}

void StatisticPanel::retranslateUi() {
     setWindowTitle(tr("Statistic"));
}
