
#include "MainWindow.h"
#include <QTextCodec>
#include <QApplication>
#include <QSettings>
#include "QtSingleApplication/QtSingleApplication"

#include <QTranslator>
#include <QLibraryInfo>

int main(int argc, char *argv[]) {
#if QT_VERSION < QT_VERSION_CHECK(5, 0, 0)
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("UTF-8"));
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));
#endif
#ifdef Q_OS_WIN
    /* на Windows используем ini-файлы, а не реестр (как по умолчанию) */
    QSettings::setDefaultFormat(QSettings::IniFormat);
#endif
    /* Приложение ltrmanager должно быть запущено всегда одно.
     * уникальность определяем по сгенерированному GUID */
    QtSingleApplication a("e36840a0-13be-4f57-ab3a-2b601deb4645", argc, argv);

    a.setOrganizationName(LTR_VENDOR_NAME);
    a.setApplicationName(PRJ_APP_TITLE);
    a.setOrganizationDomain(LTR_VENDOR_URL);
    a.setQuitOnLastWindowClosed(false);
    a.setApplicationVersion(PRJ_VERION_STR);


    if (a.isRunning())
        return !a.sendMessage("activate");

    MainWindow w;
    QObject::connect(&a, SIGNAL(messageReceived(QString)), &w, SLOT(anotherAppMsgRecvd(QString)));

    w.show();

    return a.exec();
}
