#include "CratePluginSyncCtl.h"
#include "LtrdConnection.h"
#include "LtrCrateInfo.h"
#include "Crates/SyncCtl/CrateSyncCtlDialog.h"
#include <QMessageBox>

CratePluginSyncCtl::CratePluginSyncCtl(LtrManagerEnv *env) : ActionPlugin(env) {
    env->registerActionPlugin(LtrManagerEnv::ActionTypeCrateSyncCtl, this);
}

bool CratePluginSyncCtl::actionEnabled(LtrManagerEnv *env, LtrManagerEnv::ActionType type) {
    bool en = false;
    LtrCrateInfo *crate = env->currentCrate();

    if (crate) {
        if (((crate->type()==LTR_CRATE_TYPE_LTR030) || (crate->type()==LTR_CRATE_TYPE_LTR031))
              || (crate->type() == LTR_CRATE_TYPE_LTR_CU_1) || (crate->type() == LTR_CRATE_TYPE_LTR_CEU_1)) {
            en = true;
        }
    }
    return en;
}

void CratePluginSyncCtl::actionActivate(LtrManagerEnv *env, LtrManagerEnv::ActionType type) {
    CrateSyncCtlDialog *dlg = new CrateSyncCtlDialog(env->mainWidget());
    INT err = dlg->setCrate(env->currentCrate());
    dlg->setAttribute(Qt::WA_DeleteOnClose, true);
    if (err == LTR_OK) {
        dlg->show();
    } else {
        QMessageBox::critical(env->mainWidget(), tr("Error"),
                                  tr("Cannot open crate connection") + ": " +
                                  LtrdConnection::getErrorString(err));
        delete dlg;
    }
}
