#include "CrateFirmeareUpdateEU.h"

CrateFirmeareUpdateEU::CrateFirmeareUpdateEU(TLTR030 crate) : m_crate(crate) {

}

void CrateFirmeareUpdateEU::fw_cb(DWORD stage, DWORD done_size, DWORD full_size, void* cb_data) {
    CrateFirmeareUpdateEU *obj = (CrateFirmeareUpdateEU *)(cb_data);
    obj->fwProgress(stage, done_size, full_size);
}

void CrateFirmeareUpdateEU::startDspUpdate(QString filename) {
    INT err = LTR030_LoadDspFirmware(&m_crate,
#ifdef Q_OS_WIN
                                     filename.toLocal8Bit(),
#else
                                     filename.toUtf8(),
#endif
                                     fw_cb, this);
    LTR030_Close(&m_crate);
    emit opFinish(err);
}

void CrateFirmeareUpdateEU::fwProgress(DWORD stage, DWORD done_size, DWORD full_size) {
     if ((stage==LTR030_LOAD_STAGE_WRITE) && (done_size==0))
         emit opStart(10*full_size);
     if (stage == LTR030_LOAD_STAGE_WRITE) {
         emit opProgress(tr("Firmware write"), 9*done_size);
     } else if (stage == LTR030_LOAD_STAGE_VERIFY) {
         emit opProgress(tr("Firmware verify"), 9*full_size+done_size);
     }
}
