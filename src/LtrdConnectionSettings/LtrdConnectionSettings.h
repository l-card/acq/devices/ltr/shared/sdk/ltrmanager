#ifndef LTRDCONNECTIONSETTINGS_H
#define LTRDCONNECTIONSETTINGS_H

#include <QDialog>

class LtrdConnection;

namespace Ui {
class LtrdConnectionSettings;
}

class LtrdConnectionSettings : public QDialog {
    Q_OBJECT
    
public:
    explicit LtrdConnectionSettings(LtrdConnection* con, QStringList logLvlNames, QWidget *parent = 0);
    ~LtrdConnectionSettings();


public slots:
    virtual void accept();
    
private:
    Ui::LtrdConnectionSettings *ui;

    LtrdConnection* m_con;
};

#endif // LTRDCONNECTIONSETTINGS_H
