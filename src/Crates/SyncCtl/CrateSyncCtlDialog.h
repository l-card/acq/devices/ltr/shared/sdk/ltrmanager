#ifndef CRATESYNCCTLDIALOG_H
#define CRATESYNCCTLDIALOG_H

#include <QDialog>
#include <QComboBox>

#include "ltr/include/ltrapi.h"

class LtrdConnection;
class LtrCrateInfo;

namespace Ui {
class CrateSyncCtlDialog;
}

class CrateSyncCtlDialog : public QDialog {
    Q_OBJECT

public:
    explicit CrateSyncCtlDialog(QWidget *parent);
    ~CrateSyncCtlDialog();

    int setCrate(LtrCrateInfo   *crate);

protected:
    void changeEvent(QEvent *e);
private Q_SLOTS:
    void setSyncOuts();
    void startMarkSetMode();
    void startMarkGen();
    void startSecondMarks();
    void stopSecondMarks();

    void updateDoutEnState();

private:
    void updateTitle();
    void fillUi(void);
    void fillDoutBox(QComboBox *box);
    void fillStartModeBox(QComboBox *box);
    void fillSecondModeBox(QComboBox *box);

    void fillExtDiginMarkModes(QComboBox *box);

    void setDoutBox(QComboBox *box, en_LTR_DigOutCfg outcfg);

    Ui::CrateSyncCtlDialog *ui;
    bool m_crate_open;
    TLTR hcrate;
    QString crate_name;
};

#endif // CRATESYNCCTLDIALOG_H
