#ifndef CHECKBOXITEMDELEGATE_H
#define CHECKBOXITEMDELEGATE_H


#include <QStyledItemDelegate>
#include <QModelIndex>

class CheckBoxItemDelegate : public QStyledItemDelegate  {
    Q_OBJECT
public:
    CheckBoxItemDelegate(QObject *parent);
    ~CheckBoxItemDelegate();

    void paint(QPainter *painter, const QStyleOptionViewItem &option_init, const QModelIndex &index) const;
    bool editorEvent( QEvent *event, QAbstractItemModel *model, const QStyleOptionViewItem &option, const QModelIndex &index );

    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const {return 0;}
private:
    QModelIndex m_lastClickedIndex;
};


#endif // CHECKBOXITEMDELEGATE_H
