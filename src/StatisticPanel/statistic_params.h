#ifndef STATISTIC_PARAMS_H
#define STATISTIC_PARAMS_H

/** @file statistic_params.h
    Файл содержит набор классов для отображения параметров статистики.
    Каждому классу сожержит свой класс с методами для получения имени, значения,
    подсказки и т.д.
    При выборе крейта/модуля в список просто добавляется набор элементов нужных
    классов для всех параметров, действительных для данного крейта/модуля */
#include "LtrdConnection.h"
#include "StatisticModel.h"
#include <QString>
#include <QDateTime>
#include <QColor>
#include <time.h>

static QString f_getTimeStr(quint64 timeval) {
    return
#if (QT_VERSION >= QT_VERSION_CHECK(4, 7, 0))
    QDateTime::fromMSecsSinceEpoch(timeval*1000)
#else
    QDateTime::fromTime_t((uint)timeval)
#endif
            .toString("h:mm:ss, d.MM.yyyy");
}


class LtrParam {
public:
    virtual QString name() const = 0;
    virtual QString tooltip() const {return name();}
    virtual QString statustip() const {return tooltip();}
    virtual QString whatthis() const {return tooltip();}    
};


class LtrSrvConParam : public LtrParam {
public:
    virtual QString value(LtrdConnection* con) const =0;
    virtual QColor textColor(LtrdConnection* con) const {return QColor();}
};

class LtrCrateParam : public LtrParam {
public:
    virtual QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const =0;
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const {return QColor();}
};

class LtrSlotParam : public LtrParam {
public:
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const =0;
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {return QColor();}
};


/******************** Параметры соединения с сервером ************************/
class LtrSrvParamVersion : public LtrSrvConParam {
    virtual QString name() const {return StatisticModel::tr("%1 version").arg(LTR_SERVICE_NAME);}
    virtual QString tooltip() const {return StatisticModel::tr("Version of selected %1 instance").arg(LTR_SERVICE_NAME);}
    virtual QString value(LtrdConnection *con) const {return con->versionString();}
};

class LtrSrvParamStatus : public LtrSrvConParam {
    virtual QString name() const {return StatisticModel::tr("State");}
    virtual QString tooltip() const {return StatisticModel::tr("State of connection with %1").arg(LTR_SERVICE_NAME);}
    virtual QString value(LtrdConnection *con) const {
        LtrdConnection::connStatus stat = con->status();
        switch (stat) {
            case LtrdConnection::STATUS_ONLINE:
                return StatisticModel::tr("Online");
                break;
            case LtrdConnection::STATUS_CONNECTING:
                return StatisticModel::tr("Connecting...");
                break;
            case LtrdConnection::STATUS_ERROR:
                return StatisticModel::tr("Error");
                break;
            case LtrdConnection::STATUS_OFFLINE:
                return StatisticModel::tr("Offline");
                break;
        }
        return StatisticModel::tr("Unknown state");
    }

    virtual QColor textColor(LtrdConnection* con) const {
        LtrdConnection::connStatus stat = con->status();
        switch (stat) {
            case LtrdConnection::STATUS_ONLINE:
                return Qt::darkGreen;
                break;
            case LtrdConnection::STATUS_CONNECTING:
                return Qt::darkBlue;
                break;
            case LtrdConnection::STATUS_ERROR:
                return Qt::red;
                break;
            case LtrdConnection::STATUS_OFFLINE:
                return QColor();
                break;
        }
        return QColor();
    }
};





/*********************************** Параметры крейта *************************/
class LtrCrateParamType : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Crate type");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        {return info->typeName();}
};

class LtrCrateParamSerial : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Crate serial");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        {return info->serial();}
};

class LtrCrateParamInterface : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Interface");}
    QString tooltip() const {return StatisticModel::tr("Interface used for this connection with crate");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return stat->crate_intf==LTR_CRATE_IFACE_TCPIP ? "TCP/IP" :
                 stat->crate_intf==LTR_CRATE_IFACE_USB   ? "USB" :
                 StatisticModel::tr("Unknown");}
};


class LtrCrateParamMode : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Mode");}
    QString tooltip() const {return StatisticModel::tr("Current crate mode (work/bootloader/configure)");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return stat->crate_mode==LTR_CRATE_MODE_WORK ? StatisticModel::tr("Work") :
                 stat->crate_mode==LTR_CRATE_MODE_BOOTLOADER  ? StatisticModel::tr("Bootloader") :
                 stat->crate_mode==LTR_CRATE_MODE_CONTROL     ? StatisticModel::tr("Configure only") :
                 StatisticModel::tr("Unknown");}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    {return stat->crate_mode==LTR_CRATE_MODE_WORK ? Qt::darkGreen :
                  (stat->crate_mode==LTR_CRATE_MODE_BOOTLOADER) ||
                  (stat->crate_mode==LTR_CRATE_MODE_CONTROL) ? Qt::darkBlue : Qt::red;}
};


class LtrCrateParamFirmVer : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Firmware version");}
    QString tooltip() const {return StatisticModel::tr("Crate MCU or DSP firmware version");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return info->descr().soft_ver;}
};

class LtrCrateParamBootloaderVer : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Bootloader version");}
    QString tooltip() const {return StatisticModel::tr("Crate MCU or DSP bootloader version");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return info->descr().bootloader_ver;}
};

class LtrCrateParamFpgaVer : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("FPGA version");}
    QString tooltip() const {return StatisticModel::tr("Crate FPGA firmware version");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return info->descr().fpga_version;}
};

class LtrCrateParamBoardRevision : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Revision");}
    QString tooltip() const {return StatisticModel::tr("Crate board revision");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return info->descr().brd_revision;}
};


class LtrCrateParamProtocolVersion : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Protocol version");}
    QString tooltip() const {return StatisticModel::tr("Version of protocol for crate and %1 communication").arg(LTR_SERVICE_NAME);}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    { return QString("%1.%2").arg(QString::number(info->descr().protocol_ver_major))
                .arg(QString::number(info->descr().protocol_ver_minor));}
};


class LtrCrateParamSlotsConfigVersion : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Slot config version");}
    QString tooltip() const {return StatisticModel::tr("Version of protocol for saving slot configuration (if implemented)");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    { return QString("%1.%2").arg(QString::number(info->descr().slots_config_ver_major))
                .arg(QString::number(info->descr().slots_config_ver_minor));}
};




class LtrCrateParamConTime : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Connection time");}
    QString tooltip() const {return StatisticModel::tr("Time when was established %1 connection with this crate").arg(LTR_SERVICE_NAME);}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const {
        return f_getTimeStr(stat->con_time);
    }
};


class LtrCrateParamClientCnt : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Clients");}
    QString tooltip() const {return StatisticModel::tr("Total client connections number with crate and its modules");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return QString::number(stat->total_mod_clients_cnt);}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    {return stat->total_mod_clients_cnt ? Qt::darkGreen : QColor();}
};

class LtrCrateParamCtlClientCnt : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Control connections");}
    QString tooltip() const {return StatisticModel::tr("Control client connections number with this crate");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return QString::number(stat->ctl_clients_cnt);}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    {return stat->ctl_clients_cnt ? Qt::darkGreen : QColor();}
};


class LtrCrateParamSentWrds : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Sent words");}
    QString tooltip() const {return StatisticModel::tr("Number of words sent to crate and its modules");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return QString::number(stat->wrd_sent);}
};


class LtrCrateParamRecvWrds : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Received words");}
    QString tooltip() const {return StatisticModel::tr("Number of words received from crate and its modules");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return QString::number(stat->wrd_recv);}
};


class LtrCrateParamSentBandwidth : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Send rate");}
    QString tooltip() const {return StatisticModel::tr("Total send rate of words to crate and its modules");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    { return QString::number(stat->bw_send, 'f', 1).append(" ").append(StatisticModel::tr("words/s"));}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    {return stat->bw_send ? Qt::darkGreen : QColor();}
};

class LtrCrateParamRecvBandwidth : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Receive rate");}
    QString tooltip() const {return StatisticModel::tr("Total receive rate of words from crate and its modules");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    { return QString::number(stat->bw_recv, 'f', 1).append(" ").append(StatisticModel::tr("words/s"));}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    {return stat->bw_recv ? Qt::darkGreen : QColor();}
};


class LtrCrateParamStartMarkCnt : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("'Start' marks");}
    QString tooltip() const {return StatisticModel::tr("Total number of 'Start' marks received from crate and its modules");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return QString::number(stat->total_start_marks);}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    {return stat->total_start_marks ? Qt::darkGreen : QColor();}
};

class LtrCrateParamSecondMarkCnt : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("'Second' marks");}
    QString tooltip() const {return StatisticModel::tr("Total number of 'Second' marks received from crate and its modules");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
        { return QString::number(stat->total_sec_marks);}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    {return stat->total_sec_marks ? Qt::darkGreen : QColor();}
};

class LtrCrateParamUnixtime : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Extended time mark");}
    QString tooltip() const {return StatisticModel::tr("Value of most recently received time mark with extended format (if any)");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    { return stat->crate_unixtime ? f_getTimeStr(stat->crate_unixtime) : QString();}
};


class LtrCrateParamThermL : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Thermometer (bottom)");}
    QString tooltip() const {return StatisticModel::tr("Temperature value from bottom thermometer");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    { return stat->therm_mask & (1UL << LTR_CRATE_THERM_LOWER) ? QString::number(stat->therm_vals[LTR_CRATE_THERM_LOWER], 'f', 1) + " °C" : QString();}
};

class LtrCrateParamThermH : public LtrCrateParam {
    QString name() const { return StatisticModel::tr("Thermometer (top)");}
    QString tooltip() const {return StatisticModel::tr("Temperature value from top thermometer");}
    QString value(LtrCrateInfo* info, const TLTR_CRATE_STATISTIC* stat) const
    { return stat->therm_mask & (1UL << LTR_CRATE_THERM_UPPER) ? QString::number(stat->therm_vals[LTR_CRATE_THERM_UPPER], 'f', 1) + " °C" : QString();}
};









/******************************* Параметры модуля *****************************/
class LtrSlotParamState : public LtrSlotParam {
    virtual QString name() const {return StatisticModel::tr("State");}
    virtual QString tooltip() const {return StatisticModel::tr("Selected slot state");}
    QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const  {
        int mid = mstat->mid;
        return mstat->mid == LTR_MID_EMPTY ? QColor() :
               mstat->mid == LTR_MID_INVALID ? QColor(Qt::red) :
               mstat->mid == LTR_MID_IDENTIFYING ? QColor(Qt::darkYellow) :
                                                   QColor(Qt::darkGreen);

    }
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        int mid = mstat->mid;
        if (mid == LTR_MID_EMPTY)
            return StatisticModel::tr("Empty slot");        
        if (mid == LTR_MID_IDENTIFYING)
            return StatisticModel::tr("Module ID identifying...");
        if (mid == LTR_MID_INVALID)
            return StatisticModel::tr("Invalid module ID");
        return StatisticModel::tr("Installed module") + " " + info->moduleName(slot);
    }
};


class LtrSlotParamSpeed : public LtrSlotParam {
    virtual QString name() const {return StatisticModel::tr("Interface speed mode");}
    virtual QString tooltip() const {return StatisticModel::tr("Slot interface speed mode to send words from crate to module");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return mstat->flags & LTR_MODULE_FLAGS_HIGH_BAUD ? StatisticModel::tr("High") : StatisticModel::tr("Low");
    }
};


class LtrSlotParamClientCnt : public LtrSlotParam {
    virtual QString name() const {return StatisticModel::tr("Clients");}
    virtual QString tooltip() const {return StatisticModel::tr("Client connections number with this module");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->client_cnt);
    }
};


class LtrSlotParamSentWrds : public LtrSlotParam {
    virtual QString name() const {return StatisticModel::tr("Sent words");}
    virtual QString tooltip() const {return StatisticModel::tr("Number of words sent to this module");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->wrd_sent);
    }
};

class LtrSlotParamRecvWrds : public LtrSlotParam {
    virtual QString name() const {return StatisticModel::tr("Received words");}
    virtual QString tooltip() const {return StatisticModel::tr("Number of words received from this module");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->wrd_recv);
    }
};

class LtrSlotParamRecvBandwidth : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Receive rate");}
    QString tooltip() const {return StatisticModel::tr("Receive rate of words from this module");}
    QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    { return QString::number(mstat->bw_recv, 'f', 1).append(" ").append(StatisticModel::tr("words/s"));}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    {return mstat->bw_recv ? Qt::darkGreen : QColor();}
};

class LtrSlotParamSendBandwidth : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Send rate");}
    QString tooltip() const {return StatisticModel::tr("Send rate of words from this module");}
    QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    { return QString::number(mstat->bw_send, 'f', 1).append(" ").append(StatisticModel::tr("words/s"));}
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    {return mstat->bw_send ? Qt::darkGreen : QColor();}
};


class LtrSlotParamRecvSrvBufFull : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Receive buffer");}
    QString tooltip() const {return StatisticModel::tr("Current percentage of %1 receive buffer utilizatione").arg(LTR_SERVICE_NAME);}
    QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    { return QString::number(mstat->recv_srvbuf_size > 0 ? ((double)mstat->recv_srvbuf_full)*100./mstat->recv_srvbuf_size : 0, 'f', 2).append(" %");}
};


class LtrSlotParamSendSrvBufFull : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Send buffer");}
    QString tooltip() const {return StatisticModel::tr("Current percentage of %1 send buffer utilization").arg(LTR_SERVICE_NAME);}
    QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    { return QString::number(mstat->send_srvbuf_size > 0 ? ((double)mstat->send_srvbuf_full)*100./mstat->send_srvbuf_size : 0, 'f', 2).append(" %");}
};

class LtrSlotParamRecvSrvBufFullMax : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Max. recieve buffer");}
    QString tooltip() const {return StatisticModel::tr("Maximum percentage of %1 receive buffer utilizatione").arg(LTR_SERVICE_NAME);}
    QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    { return QString::number(mstat->recv_srvbuf_size > 0 ? ((double)mstat->recv_srvbuf_full_max)*100./mstat->recv_srvbuf_size : 0, 'f', 2).append(" %");}
};


class LtrSlotParamSendSrvBufFullMax : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Max. send buffer");}
    QString tooltip() const {return StatisticModel::tr("Maximum percentage of %1 send buffer utilizatione").arg(LTR_SERVICE_NAME);}
    QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const
    { return QString::number(mstat->send_srvbuf_size > 0 ? ((double)mstat->send_srvbuf_full_max)*100./mstat->send_srvbuf_size : 0, 'f', 2).append(" %");}
};

class LtrSlotParamRBufOvs : public LtrSlotParam {
    virtual QString name() const {return StatisticModel::tr("Buffer overflows");}
    virtual QString tooltip() const {return StatisticModel::tr("Total number of %1 receive buffer overflows").arg(LTR_SERVICE_NAME);}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->rbuf_ovfls);
    }
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return mstat->rbuf_ovfls ? Qt::red : Qt::darkGreen;
    }
};

class LtrSlotParamDropWrds : public LtrSlotParam {
    virtual QString name() const {return StatisticModel::tr("Dropped words");}
    virtual QString tooltip() const {return StatisticModel::tr("Total number of dropped words for %1 receive buffer overflows").arg(LTR_SERVICE_NAME);}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->wrd_recv_drop);
    }
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return mstat->wrd_recv_drop ? Qt::red : Qt::darkGreen;
    }
};




class LtrSlotParamStartMarkCnt : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("'Start' marks");}
    QString tooltip() const {return StatisticModel::tr("Number of 'Start' marks received from this modules");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->start_mark);
    }
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return mstat->start_mark ? Qt::darkGreen : QColor();
    }
};


class LtrSlotParamSecondMarkCnt : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("'Second' marks");}
    QString tooltip() const {return StatisticModel::tr("Number of 'Second' marks received from this modules");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->sec_mark);
    }
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return mstat->sec_mark ? Qt::darkGreen : QColor();
    }
};

class LtrSlotParamSndFifoSize : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Hard fifo: size");}
    QString tooltip() const {return StatisticModel::tr("Size (in words) of internal module hardware output fifo");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->hard_send_fifo_size);
    }
};

class LtrSlotParamSndFifoFullSize : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Hard fifo: unack. words");}
    QString tooltip() const {return StatisticModel::tr("Number of words sent to module but not acknowledged");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->hard_send_fifo_unack_words);
    }
};

class LtrSlotParamSndFifoUnderrun : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Hard fifo: starvations");}
    QString tooltip() const {return StatisticModel::tr("How many times hardware fifo 'starvation' event (empty fifo when required data) occures since last reset");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->hard_send_fifo_underrun);
    }
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return mstat->hard_send_fifo_underrun ? Qt::red : Qt::darkGreen;
    }
};

class LtrSlotParamSndFifoOverrun : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Hard fifo: overflows");}
    QString tooltip() const {return StatisticModel::tr("How many times hardware fifo overflows occures since last reset");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->hard_send_fifo_overrun);
    }
    virtual QColor textColor(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return mstat->hard_send_fifo_overrun ? Qt::red : Qt::darkGreen;
    }
};


class LtrSlotParamSndFifoInternal : public LtrSlotParam {
    QString name() const { return StatisticModel::tr("Hard fifo: state");}
    QString tooltip() const {return StatisticModel::tr("Internal state of hardware output fifo showing its utilization at the crurrent moment");}
    virtual QString value(LtrCrateInfo* info, const TLTR_MODULE_STATISTIC *mstat, int slot) const {
        return QString::number(mstat->hard_send_fifo_internal);
    }
};


#endif // STATISTIC_PARAMS_H
