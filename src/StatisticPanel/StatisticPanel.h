#ifndef STATISTICPANEL_H
#define STATISTICPANEL_H

#include <QDockWidget>


class QTreeView;
class StatisticModel;
class LtrdConnection;
class LtrCrateInfo;

class StatisticPanel : public QDockWidget {
    Q_OBJECT
public:
    explicit StatisticPanel(QWidget *parent = 0);
    QTreeView* view() const {return m_view;}
    
signals:
    
public slots:
    void setItem(LtrdConnection* con, LtrCrateInfo* crate, int slot);

protected:
    void changeEvent ( QEvent * event );
private:
    void retranslateUi();

    QTreeView *m_view;
    StatisticModel *m_model;
};

#endif // STATISTICPANEL_H
