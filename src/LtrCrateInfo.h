#ifndef QLTRCRATEINFO_H
#define QLTRCRATEINFO_H

#include <QObject>
#include <QMutex>
#include <QMutexLocker>
#include <ltr/include/ltrapi.h>

class LtrdConnection;

class LtrCrateInfo : public QObject {
    Q_OBJECT
public:
    QString serial();
    QString typeName();
    quint16 type();
    QString infoString();
    int crateInterface() const;

    TLTR_CRATE_STATISTIC stat();
    TLTR_MODULE_STATISTIC mstat(int slot);
    QString moduleName(int slot);
    TLTR_CRATE_DESCR descr();

    LtrdConnection *ltrd() const {return m_con;}

    int openConnection(TLTR *hcrate);

private:
    LtrCrateInfo(int intface, const CHAR *serial, LtrdConnection *parent);
    void setStat(TLTR_CRATE_STATISTIC *stat);
    void setModStat(TLTR_MODULE_STATISTIC *mstat, int slot);

    int m_fnd;
    QMutex m_mutex;
    char m_serial[LTR_CRATE_SERIAL_SIZE];
    int m_intface;
    bool m_supportModuleNames;
    TLTR_CRATE_DESCR m_descr;
    TLTR_CRATE_STATISTIC m_stat;
    TLTR_MODULE_STATISTIC m_mod_stat[LTR_MODULES_PER_CRATE_MAX];
    LtrdConnection *m_con;

    friend class LtrdConnection;

};

#endif // QLTRCRATEINFO_H
