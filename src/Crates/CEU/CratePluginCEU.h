#ifndef CRATEPLUGINCEU_H
#define CRATEPLUGINCEU_H

#include "ActionPlugin.h"

class CratePluginCEU : public ActionPlugin {
    Q_OBJECT
public:
    CratePluginCEU(LtrManagerEnv *env);

    bool actionEnabled(LtrManagerEnv *env, LtrManagerEnv::ActionType type);
    void actionActivate(LtrManagerEnv *env, LtrManagerEnv::ActionType type);


private:
    void updateFirmware(LtrManagerEnv *env);
    void reset(LtrManagerEnv *env);
    void config(LtrManagerEnv *env);
};

#endif // CRATEPLUGINCEU_H
