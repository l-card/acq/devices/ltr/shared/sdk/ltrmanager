#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMenu>
#include <QTranslator>
#include "LtrCratesModel.h"
#include "LtrManagerEnv.h"

namespace Ui {
class MainWindow;
}

class IpEntriesPanel;
class StatisticPanel;
class LtrdLogPanel;
class QModelIndex;
class QIcon;


class MainWindow : public QMainWindow {
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void addConnection(LtrdConnection* con, bool autocon);
    void startCurConnection();
    void stopCurConnection();

    void anotherAppMsgRecvd(const QString &string);
    
private:
    Ui::MainWindow *ui;
    QSystemTrayIcon* tray;

    IpEntriesPanel* m_ipEntriesPanel;
    StatisticPanel* m_statPanel;
    LtrdLogPanel* m_logPanel;


    QByteArray m_winState;
    QSize m_size;
    QPoint m_pos;
    QMenu m_crateContextMenu;

    QIcon m_icon_ok;
    QIcon m_icon_no_crates;
    QIcon m_icon_idle;
    QIcon m_icon_err;

    QTranslator m_translator;
    QTranslator m_translatorLboot;
    QTranslator m_translatorQt;
    QString     m_curLangName;

    LtrManagerEnv m_env;

    void getCurConInfo(LtrdConnection **con, LtrCrateInfo **crate=0, int *slot=0, TLTR_MODULE_STATISTIC* mstat=0);
    void createLanguageMenu();
protected:
    void closeEvent ( QCloseEvent * event );
    bool event ( QEvent * event );
    void changeEvent ( QEvent * event );

private slots:
    void retranslateUi();

    void trayActivated(QSystemTrayIcon::ActivationReason reason);
    void on_actionQuit_triggered();
    void crateInfoChanged(LtrCrateInfo* info,LtrConEvent event);
    void ltrdStatusChanged(LtrdConnection::connStatus status);
    void hideToTray();
    void saveSettings();
    void savePosition();
    void crateSelChanged(QModelIndex current, QModelIndex previous);

    void addedCrateRow(const QModelIndex &index, int x, int y);

    void resetCurModule();
    void showCratesTreeMenu(QPoint point);

    void addLtrdConnectionDialog(void);
    void remLtrdConnection();
    void ltrdConnectionSettingsDialog();
    void LtrdAutoconChanged();
    void setIconStatus();

    void showHideMainWindow(bool hide_enable = true);

    void onLanguageChanged(QAction*act);
    void setLanguage(QString name);
    void switchTranslator(QTranslator &translator, QLocale &locale, QString name, QString dir);
};

#endif // MAINWINDOW_H
