#ifndef ADDLTRDCONNECTIONDIALOG_H
#define ADDLTRDCONNECTIONDIALOG_H

#include <QDialog>
#include <QHostAddress>

namespace Ui {
class AddLtrdConnectionDialog;
}

class AddLtrdConnectionDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit AddLtrdConnectionDialog(QWidget *parent = 0);
    ~AddLtrdConnectionDialog();


    QHostAddress addr() const;
    bool autocon() const;
    bool needConnect() const;
    bool remote() const;
    
private slots:
    void ipTextChanged(QString text);
    void remoteCheckChanged();

private:


    Ui::AddLtrdConnectionDialog *ui;
};

#endif // ADDLTRDCONNECTIONDIALOG_H
