#ifndef CRATEPLUGINEU_H
#define CRATEPLUGINEU_H

#include "ActionPlugin.h"


class CrateFirmeareUpdateEU;
class QProgressDialog;
class QThread;

class CratePluginEU : public ActionPlugin {
    Q_OBJECT
public:
    CratePluginEU(LtrManagerEnv *env);
    ~CratePluginEU();

    bool actionEnabled(LtrManagerEnv *env, LtrManagerEnv::ActionType type);
    void actionActivate(LtrManagerEnv *env, LtrManagerEnv::ActionType type);

    void config(LtrManagerEnv *env);
    void updateFirmware(LtrManagerEnv *env);
    void reset(LtrManagerEnv *env);

signals:
    void startDspUpdate(QString filename);
private slots:
    void opStarted(unsigned full_size);
    void opProgress(QString stage, unsigned done);
    void opFinished(int err);
private:
    CrateFirmeareUpdateEU *m_fwUpdater;
    QThread *m_fwThread;
    LtrManagerEnv *m_env;
    QProgressDialog *m_progressDialog;

};

#endif // CRATEPLUGINEU_H
