#ifndef CRATEPLUGINSYNCCTL_H
#define CRATEPLUGINSYNCCTL_H

#include "ActionPlugin.h"

class CratePluginSyncCtl : public ActionPlugin {
    Q_OBJECT
public:
    CratePluginSyncCtl(LtrManagerEnv *env);

    bool actionEnabled(LtrManagerEnv *env, LtrManagerEnv::ActionType type);
    void actionActivate(LtrManagerEnv *env, LtrManagerEnv::ActionType type);
};

#endif // CRATEPLUGINSYNCCTL_H
