#ifndef LTRDLOGMODEL_H
#define LTRDLOGMODEL_H

#include <QAbstractTableModel>
#include <QColor>

#include "LtrdConnection.h"

class LtrdLogModel : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit LtrdLogModel(QObject *parent = 0);

    class logLvlCfg {
    public:
        logLvlCfg(en_LTR_LogLevel lvl, QColor color, QString text) :
            m_lvl(lvl), m_color(color), m_text(text) {}

        en_LTR_LogLevel level() const {return m_lvl;}
        QString name() const {return m_text;}
        QColor color() const {return m_color;}
    private:
        en_LTR_LogLevel m_lvl;
        QColor m_color;
        QString m_text;
    } ;


    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                         int role = Qt::DisplayRole ) const;




    int logLevelCnt() const;
    logLvlCfg logLevelConfig(int idx) const;
    en_LTR_LogLevel curLogLevel() const {return m_lvl; }

public Q_SLOTS:
    void setLogLevel(en_LTR_LogLevel lvl);
    void retranslate();
    void setConnection(LtrdConnection *con);
    void clear();
private Q_SLOTS:
    void addLogRecords(QList<QSharedPointer<LtrSrvLogRec> > recs);
    void removeOldLogRecords(QList<QSharedPointer<LtrSrvLogRec> > recs);


    void redrawLogRecords();
private:
    static const int COLUMN_TIME   = 0;
    static const int COLUMN_LEVEL  = 1;
    static const int COLUMN_MSG    = 2;
    static const int COLUMN_CNT    = 3;


    logLvlCfg getLvlCfg(en_LTR_LogLevel level) const;


    QList<QSharedPointer<LtrSrvLogRec> > m_showlogrec;
    en_LTR_LogLevel m_lvl;

    QList<logLvlCfg> m_logLvlCfg;

    LtrdConnection *m_con;

};

#endif // LTRDLOGMODEL_H
