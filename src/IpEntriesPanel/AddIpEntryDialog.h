#ifndef ADDIPENTRYDIALOG_H
#define ADDIPENTRYDIALOG_H

#include <QDialog>
#include <QHostAddress>
class QPushButton;
namespace Ui {
class AddIpEntryDialog;
}

class AddIpEntryDialog : public QDialog {
    Q_OBJECT
    
public:
    explicit AddIpEntryDialog(QWidget *parent = 0, bool permVisible = true);
    ~AddIpEntryDialog();

    QHostAddress addr() const;
    bool autocon() const;
    bool reconnect() const;
    bool needConnect() const;
    bool permanent() const;
    
private:
    Ui::AddIpEntryDialog *ui;
private slots:
    void ipTextChanged(QString text);
};

#endif // ADDIPENTRYDIALOG_H
