#ifndef CRATECONFIGDIALOG_H
#define CRATECONFIGDIALOG_H

#include <QDialog>
#include "LtrCrateInfo.h"

class QLineEdit;
class QCheckBox;

namespace Ui {
class CrateConfigDialog;
}

class CrateConfigDialog : public QDialog {
    Q_OBJECT
public:
    struct Config {
        static const int MAC_ADDR_LEN = 6;

        quint8 iface;                      /* Выбор интерфейса (CONF_IFACE_...) */        
        quint32 ip_addr;
        quint32 ip_netmask;
        quint32 ip_gateway;
        bool    dhcp_en;
        quint8  factory_mac[MAC_ADDR_LEN];
        bool    user_mac_en;
        quint8  user_mac[MAC_ADDR_LEN];

    };

    enum FeaturesFlags {
        FlagPasswordRequired = 0x00000001,
        FlagShowCrateReset   = 0x00000002,
        FlagShowFactoryMac    = 0x00000004,
        FlagShowUserMac      = 0x00000008,
        FlagShowDHCP         = 0x00000010,
        FlagShowSetPassword  = 0x00000020

    } ;


    explicit CrateConfigDialog(Config &cfg,  QWidget *parent, int flags);
    ~CrateConfigDialog();



    quint8 crateInterface() const;
    quint32 ipAddr() const;
    quint32 ipMask() const;
    quint32 ipGate() const;

    QString     passwd() const;
    bool        useDhcp() const;
    bool        setUserMac() const;
    QByteArray  userMac() const;

    bool        setNewPass() const;
    QString     newPass() const;


    static char macSeparator() {return ':';}
public slots:
    virtual void accept();
private slots:
    void dataChanged(void);
private:
    QString macStr(const quint8 *mac);




    bool m_modified;
    int m_flags;
    QString m_pass;
    Ui::CrateConfigDialog *ui;

    QLineEdit *newPassEdit, *newPassRepeatEdit;
    QLineEdit *userMacEdit, *factoryMacEdit;
    QCheckBox *userMacCheck, *dhcpEnCheck, *newPassSetCheck;
};

#endif // CRATECONFIGDIALOG_H
