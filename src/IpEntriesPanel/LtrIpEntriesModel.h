#ifndef LTRIPENTRIESMODEL_H
#define LTRIPENTRIESMODEL_H

#include <QAbstractTableModel>
#include "LtrdConnection.h"
class QTableView;

/** Модель для списка адресов сервера.
    Ловит сигналы от соединения с сервером о появлении, исчезновении и изменении
    записей с IP-адресами крейта и предоставляет стандартный интерфейс для
    для отображения данных о текущих записях представлением */
class LtrIpEntriesModel : public QAbstractTableModel {
    Q_OBJECT
public:
    explicit LtrIpEntriesModel(LtrdConnection *con, QObject *parent = 0);
    int ipEntryCount() const;
    TLTR_CRATE_IP_ENTRY ipEntry(int ind) const;

    
    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    QVariant headerData (int section, Qt::Orientation orientation,
                         int role = Qt::DisplayRole ) const;
    bool setData (const QModelIndex & index, const QVariant & value, int role = Qt::EditRole);


    void initView(QTableView *view);
signals:
    void ipEntryFlagsChange(qint32 addr,  qint32 flags, bool perm);
public slots:
    

private:

    static const int COLUMN_AUTO = 0;
    static const int COLUMN_RECONNECT = 1;
    static const int COLUMN_ADDR = 2;
    static const int COLUMN_STATUS = 3;
    static const int COLUMN_SERIAL = 4;
    static const int COLUMN_CNT = 5;


    QList<TLTR_CRATE_IP_ENTRY> m_ip_list;
    LtrdConnection *m_con;
private Q_SLOTS:
    void ipAddrStatusChanged(TLTR_CRATE_IP_ENTRY ip, LtrConEvent event, int pos);    
};

#endif // LTRIPENTRIESMODEL_H
