#ifndef LTRSRVLOGPANEL_H
#define LTRSRVLOGPANEL_H

#include <QDockWidget>
#include <QTimer>

#include "LtrdLogModel.h"

#include <QDateTime>
#include <QPoint>
#include <QColor>

class QPushButton;
class QAction;
class QMenu;
class QFrame;
class QTableView;
class LQTableCopyAction;

class LtrdLogPanel : public QDockWidget {
    Q_OBJECT
public:
    explicit LtrdLogPanel(QWidget *parent = 0);
    ~LtrdLogPanel();

    QStringList logLevelNames();

public slots:
    /** передача активного соединения с сервером, список адресов которого
        должен быть отображен */
    void setConnection(LtrdConnection *con);


    void clear(void);
    void setLogLevel(en_LTR_LogLevel lvl);

    void resizeRows();
protected:
    void changeEvent ( QEvent * event );
private slots:
    void showContextMenu(QPoint point);
    void lvlActTriggered();
    void retranslateUi();

    void checkScroll();
    void onInserted(const QModelIndex &, int , int last);


private:
    QPushButton *m_clearButton;
    QTableView *m_logbox;
    QAction *m_actClear, *m_actSelectAll, *m_actResize;
    LQTableCopyAction *m_actCopy;
    QMenu *m_contextMenu;
    QMenu *m_levelMenu;



    QHash<QAction*, int> m_logLvlActMap;


    void showLogRec(LtrSrvLogRec *entry);
    LtrdLogModel *m_model;
};

#endif // LTRSRVLOGPANEL_H
